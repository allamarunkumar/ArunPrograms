/*Write a program that creates three variables: an int, a double, and a String.

Put the value 113 into the first variable, the value 2.71828 into the second, and the value "Computer Science" into the third. It does not matter what you call the variables... this time.

Then, display the values of these three variables on the screen, one per line.*/
public class UsingVariables {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int RoomNo=113;
		double Cl=2.71828;
		String Subject="Computer Science";
		System.out.println("This is room #  "+RoomNo);
		System.out.println("e is close to "+Cl); 
		System.out.println("I am learning a bit about "+Subject); 

	}

}
