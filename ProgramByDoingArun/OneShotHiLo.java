import java.util.Scanner;
import java.util.Random;

public class OneShotHiLo {

	
	public static void main(String[] args) 
	{
		System.out.println("I'm thinking of a number between 1-100.  Try to guess it.");
		System.out.print(">");
		Scanner Sc=new Scanner(System.in);
		int Guess=Sc.nextInt();
		Random rn = new Random();
		int answer = rn.nextInt(100);
		if(Guess>answer)
		{
			System.out.println("Sorry, you are too high.  I was thinking of "+answer);
		}
		else if(Guess<answer)
		{
			System.out.println("Sorry, you are too low.  I was thinking of "+answer);
		}
		else
		{
			System.out.println("You guessed it!  What are the odds?!?");
		}
	
		

	}

	

}
