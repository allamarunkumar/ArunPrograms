import java.util.Scanner;


public class CopyingArrays {

	/*Create an array of ten integers
Fill the array with ten random numbers (1-100)
Copy the array into another array of the same capacity
Change the last value in the first array to a -7
Display the contents of both arrays*/
	
	public static void main(String[] args) 
	{
		int[] a={56,78,34,32,33,2,1,4,5,10};
		int[] b=new int[10];
		for(int i=0;i<10;i++)
		{
			b[i]=a[i];
			
		}
		a[9]=-7;
		System.out.print("Array 1:  " );
		for(int i=0;i<10;i++)
		{
			System.out.print(a[i]+"\t");
			
		}
			
		
		System.out.println("\n");
		System.out.print("Array 2:  " );
		for(int i=0;i<10;i++)
		{
			System.out.print(b[i]+"\t");
		}
		
	}

}
