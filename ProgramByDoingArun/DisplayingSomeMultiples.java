import java.util.Scanner;


public class DisplayingSomeMultiples 
{

	
	public static void main(String[] args) 
	{
		int n;
		System.out.print("Choose a Number:");
		Scanner Sc=new Scanner(System.in);
		n=Sc.nextInt();
		for(int i=1;i<=12;i++)
		{
			System.out.println(n+"*"+i+"="+n*i);
		}

	}

}
