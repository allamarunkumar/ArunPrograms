import java.util.Scanner;


public class AddingValuewithaForLoop {

	public static void main(String[] args) 
	{
		Scanner Sc=new Scanner(System.in);
		System.out.print("Number:");
		int n=Sc.nextInt();
		int sum=0;
		for(int i=1;i<=n;i++)
		{
			System.out.print(i+"  ");
			sum+=i;
		}
    System.out.println("\nThe  Sum is:"+sum);
	}

}
