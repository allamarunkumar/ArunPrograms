import java.util.Scanner;


public class Letter_atTime {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		System.out.print("What is your message?:");
		Scanner Sc=new Scanner(System.in);
		String Msg=Sc.nextLine();
		int k=Msg.length();
		
		System.out.println("\nYour message is " + k + " characters long.");
		System.out.println("The first character is at position 0 and is '" + Msg.charAt(0) + "'.");
		int lastpos = k - 1;
		System.out.println("The last character is at position " + lastpos + " and is '" + Msg.charAt(lastpos) + "'.");
		System.out.println("\nHere are all the characters, one at a time:\n");
		for ( int i=0; i<k; i++ )
		{
			System.out.println("\t" + i + " - '" + Msg.charAt(i) + "'");
		}

		int Vowels_count = 0;

		for ( int i=0; i<k; i++ )
		{
			char letter = Msg.charAt(i);
			if ( letter == 'a' || letter == 'A'|| letter == 'e' || letter == 'E'||letter == 'i' || letter == 'I'||letter == 'o' || letter == 'O'||letter == 'u' || letter == 'U')
			{
				Vowels_count++;
			}
		}

		System.out.println("\nYour message contains the Vowels " + Vowels_count + " times. Isn't that interesting?");


	}

}
