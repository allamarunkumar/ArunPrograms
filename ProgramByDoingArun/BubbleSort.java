import java.util.Random;
import java.util.Scanner;


public class BubbleSort 
{

	public static void main(String[] args) 
	{
	
		int[]a=new int[10];
		Scanner Sc=new Scanner(System.in);
		Random R=new Random();
		for(int i=0;i<=9;i++)
		{
			a[i]=R.nextInt(1000);
		}
		for(int k:a)
		{
			System.out.println(k);
		}
		BubbleSort(a);
		System.out.println("Elements after Swapping");
		for(int k:a)
		{
			System.out.println(k);
		}
		BubbleSort(a);
	}
	public static void BubbleSort(int[]x)
	{
		for(int i=0;i<x.length;i++)
		{
			for(int j=i+1;j<x.length;j++)
			{
				if(x[i]>x[j])
				{
					Swap(x,i,j);
				}
				
			}
		}
	}
	public static void Swap(int[]a,int i,int j)
	{
		int temp=a[i];
		a[i]=a[j];
		a[j]=temp;
	}

}
